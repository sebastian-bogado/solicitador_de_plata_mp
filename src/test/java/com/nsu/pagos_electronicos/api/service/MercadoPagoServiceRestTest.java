package com.nsu.pagos_electronicos.api.service;

import com.nsu.pagos_electronicos.api.dto.SolicitudDineroRequestDTO;
import com.nsu.pagos_electronicos.core.model.CurrencyEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import java.math.BigDecimal;

/**
 * Created by sbogado on 7/11/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MercadoPagoServiceRestTest {


    @Autowired
    private MercadoPagoServiceRest mercadoPagoService;

    @Test
    public void getToken(){
        System.out.println(this.mercadoPagoService.generateToken().toString());
    }

    @Test
    public void create(){
        SolicitudDineroRequestDTO solicitudDineroRequestDTO = new SolicitudDineroRequestDTO();
        solicitudDineroRequestDTO.setConcept("off_platform");
        solicitudDineroRequestDTO.setCurrency(CurrencyEnum.ARS);
        solicitudDineroRequestDTO.setDescription("Hola mundo");
        solicitudDineroRequestDTO.setPayerEmail("test_user_56966833@testuser.com");
        solicitudDineroRequestDTO.setAmount(new BigDecimal(21.5));
        try{
            mercadoPagoService.createMoneyRequest(solicitudDineroRequestDTO);
        }catch(HttpClientErrorException e){
            System.out.println(e.getResponseBodyAsString());
        }

    }

}
