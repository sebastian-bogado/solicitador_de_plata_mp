package com.nsu.pagos_electronicos.rest.api;

import com.nsu.pagos_electronicos.rest.api.dto.TransactionDTO;
import com.nsu.pagos_electronicos.rest.api.dto.TransactionElementDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CreateProductControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    private static final Logger LOG = LoggerFactory.getLogger(CreateProductControllerTest.class);

    /**
     * Create a simple transaction and response the mercadopago response or similar
     *
     */
    @Test
    public void testCreateTransaction(){
        TransactionDTO requestObject = new TransactionDTO();
        richTransaction(requestObject);
        TransactionDTO response = restTemplate.postForObject("/", requestObject, TransactionDTO.class);
        LOG.debug(response.toString());
    }

    private void richTransaction(TransactionDTO requestObject) {
        requestObject.setEmail("seebogado@gmail.com");
        requestObject.setElementsList(mockElementList());
    }

    private List<TransactionElementDTO> mockElementList() {
        return IntStream.range(0, 6).mapToObj(index -> createElementDTO()).collect(Collectors.toList());
    }

    private TransactionElementDTO createElementDTO() {
        Random random = new Random(new Date().getTime());
        TransactionElementDTO transactionElementDTO = new TransactionElementDTO();
        transactionElementDTO.setAmmount(new BigDecimal(Math.abs(random.nextDouble())));
        transactionElementDTO.setDetalle("Detalle random "+ random.nextInt());
        transactionElementDTO.setQuantity(random.nextInt());
        return transactionElementDTO;
    }

}
