package com.nsu.pagos_electronicos.handlers;

import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * In this class we handle the BusinessException sending a response with this stucture:
 *  code:       This is the exception code to find into messages.properties and later search the messages.properties code into the java code.
 *              Always you show this for make a diagnostic
 *
 *  message:    This is the message to show the user. This message should to be a friendly message
 *
 *  status:     This is the HTTP status. For more information read the .README
 */
@RestControllerAdvice
public class BusinessExceptionHandlers {
}
