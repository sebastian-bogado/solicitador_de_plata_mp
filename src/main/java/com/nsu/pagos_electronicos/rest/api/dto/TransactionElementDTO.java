package com.nsu.pagos_electronicos.rest.api.dto;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;


public class TransactionElementDTO {

    private String detalle;

    @NotNull
    private BigDecimal ammount;

    @NotNull
    private Integer quantity;


    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public BigDecimal getAmmount() {
        return ammount;
    }

    public void setAmmount(BigDecimal ammount) {
        this.ammount = ammount;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "TransactionElementDTO{" +
                "detalle='" + detalle + '\'' +
                ", ammount=" + ammount +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransactionElementDTO that = (TransactionElementDTO) o;

        if (detalle != null ? !detalle.equals(that.detalle) : that.detalle != null) return false;
        if (ammount != null ? !ammount.equals(that.ammount) : that.ammount != null) return false;
        return quantity != null ? quantity.equals(that.quantity) : that.quantity == null;
    }

    @Override
    public int hashCode() {
        int result = detalle != null ? detalle.hashCode() : 0;
        result = 31 * result + (ammount != null ? ammount.hashCode() : 0);
        result = 31 * result + (quantity != null ? quantity.hashCode() : 0);
        return result;
    }
}
