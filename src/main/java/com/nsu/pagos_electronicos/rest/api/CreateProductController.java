package com.nsu.pagos_electronicos.rest.api;

import com.nsu.pagos_electronicos.core.exception.BuyNotFoundExceptionById;
import com.nsu.pagos_electronicos.core.model.Transaction;
import com.nsu.pagos_electronicos.core.service.TransactionService;
import com.nsu.pagos_electronicos.rest.api.dto.TransactionDTO;
import com.nsu.pagos_electronicos.util.MapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//TODO change the ids for a char[32]
@RestController
@RequestMapping(value= "/order", produces="application/json", consumes = "application/json")
public class CreateProductController {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private MapperUtils mapperUtils;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public TransactionDTO createTransactionService(@RequestBody TransactionDTO request){
        Transaction transaction = mapperUtils.mapCreateTransactionRequestDTOToTransaction(request);
        TransactionDTO response = mapperUtils.mapTransactionToTransactionDTO(transactionService.create(transaction));
        return response;
    }


    @RequestMapping(value = "/{transactionUuid}", method = RequestMethod.GET)
    public TransactionDTO getTransaction(@PathVariable("transactionUuid")Long transactionId) throws BuyNotFoundExceptionById {
        return mapperUtils.mapTransactionToTransactionDTO(transactionService.findTransactionById(transactionId));
    }

}
