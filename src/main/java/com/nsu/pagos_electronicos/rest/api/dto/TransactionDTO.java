package com.nsu.pagos_electronicos.rest.api.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
@Data
public class TransactionDTO {

    private Long id;

    @NotNull
    private LocalDateTime date;

    @NotNull
    private String status;

    private List<TransactionElementDTO> elementsList;

    @NotNull @NotEmpty
    private String email;

}
