package com.nsu.pagos_electronicos.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * Created by sbogado on 7/10/17.
 */
@Configuration
public class RestConsumerConfiguration{

    @Bean
    public RestTemplate getRestTemplate(){
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate;

    }

}