package com.nsu.pagos_electronicos.util;

import com.nsu.pagos_electronicos.core.model.StatusEnum;
import com.nsu.pagos_electronicos.core.model.Transaction;
import com.nsu.pagos_electronicos.core.model.TransactionElement;
import com.nsu.pagos_electronicos.rest.api.dto.TransactionDTO;
import com.nsu.pagos_electronicos.rest.api.dto.TransactionElementDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MapperUtils {
    public Transaction mapCreateTransactionRequestDTOToTransaction(TransactionDTO request) {
        Transaction transaction= new Transaction();
        transaction.setStatus(StatusEnum.getStatusEnumFromString(request.getStatus()));
        transaction.setDate(request.getDate());
        transaction.setElementsList(this.mapCreateTransactionElementsDTOToTransactionElements(request.getElementsList()));
        transaction.setId(request.getId());
        return transaction;
    }

    public List<TransactionElement> mapCreateTransactionElementsDTOToTransactionElements(List<TransactionElementDTO> elements) {
        List<TransactionElement> transactionElements = new ArrayList<>();
        elements.forEach(element -> {
                                        transactionElements.add(mapCreateTransactionElementDTOToTransactionElement(element));
        });
        return transactionElements;
    }

    public TransactionElement mapCreateTransactionElementDTOToTransactionElement(TransactionElementDTO element) {
        TransactionElement transactionElement = new TransactionElement();
        transactionElement.setAmmount(element.getAmmount());
        transactionElement.setDetalle(element.getDetalle());
        transactionElement.setQuantity(element.getQuantity());
        return transactionElement;
    }

    public TransactionDTO mapTransactionToTransactionDTO(Transaction transaction) {
        TransactionDTO response = new TransactionDTO();
        response.setId(transaction.getId());
        response.setStatus(transaction.getStatus().name());
        response.setDate(transaction.getDate());
        response.setEmail(transaction.getEmail());
        response.setElementsList(mapTransactionElementsToTransactionElementsDTO(transaction.getElementsList()));
        return response;
    }



    public List<TransactionElementDTO> mapTransactionElementsToTransactionElementsDTO(List<TransactionElement> elements) {
        List<TransactionElementDTO> transactionElements = new ArrayList<>();
        elements.forEach(element -> {
            transactionElements.add(mapTransactionElementToTransactionElementDTO(element));
        });
        return transactionElements;
    }

    public TransactionElementDTO mapTransactionElementToTransactionElementDTO(TransactionElement element) {
        TransactionElementDTO transactionElementDTO = new TransactionElementDTO();
        transactionElementDTO.setAmmount(element.getAmmount());
        transactionElementDTO.setDetalle(element.getDetalle());
        transactionElementDTO.setQuantity(element.getQuantity());
        return transactionElementDTO;
    }



}
