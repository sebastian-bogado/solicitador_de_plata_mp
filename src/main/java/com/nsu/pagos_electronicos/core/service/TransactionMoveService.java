package com.nsu.pagos_electronicos.core.service;

import com.nsu.pagos_electronicos.core.model.TransactionMove;

/**
 * Created by sbogado on 7/11/17.
 */
public interface TransactionMoveService {

    TransactionMove create(TransactionMove transactionMove);
    TransactionMove update(TransactionMove transactionMove);
    void delete(TransactionMove transactionMove);
    void delete(Long id);
    void getMovesByTransactionId(Long transactionId);

}
