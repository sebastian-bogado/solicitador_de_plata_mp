package com.nsu.pagos_electronicos.core.service;

import com.nsu.pagos_electronicos.core.exception.BuyNotFoundExceptionById;
import com.nsu.pagos_electronicos.core.model.StatusEnum;
import com.nsu.pagos_electronicos.core.model.Transaction;

public interface TransactionService {

    Transaction create(Transaction transaction);
    Transaction updateBuy(Transaction transaction);
    Transaction findTransactionById(Long id) throws BuyNotFoundExceptionById;
    Transaction updateBuy(Long id, StatusEnum statusEnum) throws BuyNotFoundExceptionById;



}
