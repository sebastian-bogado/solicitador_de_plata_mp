package com.nsu.pagos_electronicos.core.service.impl;

import com.nsu.pagos_electronicos.core.dao.TransactionMoveDao;
import com.nsu.pagos_electronicos.core.model.TransactionMove;
import com.nsu.pagos_electronicos.core.service.TransactionMoveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by sbogado on 7/10/17.
 */
@Service
public class TransactionMoveServiceImplDB implements TransactionMoveService {

    @Autowired
    private TransactionMoveDao transactionMoveDao;

    @Override
    public TransactionMove create(TransactionMove transactionMove){
        return transactionMoveDao.save(transactionMove);
    }

    @Override
    public TransactionMove update(TransactionMove transactionMove){
        //TODO ver que carajo hago aca?
        return transactionMoveDao.save(transactionMove);
    }

    @Override
    public void delete(TransactionMove transactionMove){
        this.transactionMoveDao.delete(transactionMove);
    }

    @Override
    public void delete(Long id){
        this.transactionMoveDao.delete(id);
    }

    @Override
    public void getMovesByTransactionId(Long transactionId){
        this.transactionMoveDao.findAllByTransaction_Id(transactionId);
    }



}
