package com.nsu.pagos_electronicos.core.service;

import com.nsu.pagos_electronicos.core.dao.TransactionDao;
import com.nsu.pagos_electronicos.core.exception.BuyNotFoundExceptionById;
import com.nsu.pagos_electronicos.core.model.Transaction;
import com.nsu.pagos_electronicos.core.model.StatusEnum;
import com.nsu.pagos_electronicos.core.model.TransactionMove;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Created by sbogado on 7/4/17.
 */
@Service
public class TransactionServiceImplDB implements TransactionService{

    @Autowired
    private TransactionDao transactionDao;

    @Autowired
    private TransactionMoveService transactionMoveService;

    @Override
    public Transaction create(Transaction transaction){
        TransactionMove transactionMove = new TransactionMove();
        transactionMove.setActualStatus(StatusEnum.CREATED);
        transactionMove.setTransaction(transaction);
        transactionMove.setMoveDate(LocalDateTime.now());



        return transactionDao.save(transaction);
    }

    @Override
    public Transaction updateBuy(Transaction transaction){
        return transactionDao.save(transaction);
    }

    @Override
    public Transaction findTransactionById(Long id) throws BuyNotFoundExceptionById {
        Optional<Transaction> optBuy = transactionDao.findTransactionById(id);
        if(!optBuy.isPresent()){
            throw new BuyNotFoundExceptionById(id);
        }
        return optBuy.get();
    }

    @Override
    public Transaction updateBuy(Long id, StatusEnum statusEnum) throws BuyNotFoundExceptionById {
        Transaction transaction = this.findTransactionById(id);
        transaction.setStatus(statusEnum);



        return transactionDao.save(transaction);
    }

}
