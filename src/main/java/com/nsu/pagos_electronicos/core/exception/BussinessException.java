package com.nsu.pagos_electronicos.core.exception;

/**
 * Created by sbogado on 7/4/17.
 */
public abstract  class BussinessException extends Exception {

    private CustomError error;


    public BussinessException(CustomError error) {
        this.error = error;
    }


    public BussinessException(String message, Throwable cause, CustomError error) {
        super(message, cause);
        this.error = error;
    }


    public CustomError getError() {
        return error;
    }

}