package com.nsu.pagos_electronicos.core.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by sbogado on 7/4/17.
 */
public class CustomError {
    private String code;

    private String message;

    private HttpStatus status;

    public CustomError(String code, String message, HttpStatus status) {
        this.code = code;
        this.message = message;
        this.status = status;
    }

    public CustomError(String code, String message) {
        this.code = code;
        this.message = message;
        this.status = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    public CustomError(String code, HttpStatus status) {
        this.code = code;
        this.status = status;
        this.message = "";
    }

    public CustomError() {
        this.code = "";
        this.status = HttpStatus.INTERNAL_SERVER_ERROR;
        this.message = "";
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getStatus() {
        return status;
    }

}
