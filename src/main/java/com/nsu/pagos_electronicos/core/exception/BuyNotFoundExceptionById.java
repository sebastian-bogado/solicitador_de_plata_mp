package com.nsu.pagos_electronicos.core.exception;

/**
 * Created by sbogado on 7/4/17.
 */
public class BuyNotFoundExceptionById extends BussinessException {

    public BuyNotFoundExceptionById(Long Id) {
        //TODO: add params to this instance
        super(new CustomError());
    }

    public BuyNotFoundExceptionById(String message, Throwable cause, Long id) {
        super(message, cause, new CustomError());
    }
}
