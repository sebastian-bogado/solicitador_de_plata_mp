package com.nsu.pagos_electronicos.core.dao;

import com.nsu.pagos_electronicos.core.model.TransactionMove;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sbogado on 7/4/17.
 */
@Repository
public interface TransactionMoveDao  extends JpaRepository<TransactionMove, Long>{

    List<TransactionMove> findAllByTransaction_Id(Long transactionId);
}
