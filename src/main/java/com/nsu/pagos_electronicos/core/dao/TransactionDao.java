package com.nsu.pagos_electronicos.core.dao;

import com.nsu.pagos_electronicos.core.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 *
 * Persist and different queries for buy table
 * This have information for the order
 *
 */
@Repository
public interface TransactionDao extends JpaRepository<Transaction, Long>{

    Optional<Transaction> findTransactionById(Long id);

}
