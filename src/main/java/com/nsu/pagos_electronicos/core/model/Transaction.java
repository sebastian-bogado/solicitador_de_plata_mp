package com.nsu.pagos_electronicos.core.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * This class is for persist the buy or transaction information
 *  id:
 *  date: Transaction date
 *  status: Transaction status
 *  elementsList:
 *
 */
@Entity
@Data
public class Transaction {

    @Id @GeneratedValue
    private Long id;

    @NotNull
    private LocalDateTime date;

    @Enumerated(EnumType.STRING)
    private StatusEnum status;


    @OneToMany(fetch = FetchType.EAGER)
    private List<TransactionElement> elementsList;

    @NotNull @NotEmpty
    private String email;


    private String mercadoPagoId;

    @OneToMany(mappedBy = "transaction")
    private List<TransactionMove> moves;

}
