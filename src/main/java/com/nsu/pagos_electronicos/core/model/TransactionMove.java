package com.nsu.pagos_electronicos.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Created by sbogado on 7/3/17.
 */
@Entity
public class TransactionMove {


    @Id
    @GeneratedValue
    private Long id;

    @Enumerated
    private StatusEnum previousStatus;

    @Enumerated
    private StatusEnum actualStatus;

    @NotNull
    private LocalDateTime moveDate;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Transaction transaction;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StatusEnum getPreviousStatus() {
        return previousStatus;
    }

    public void setPreviousStatus(StatusEnum previousStatus) {
        this.previousStatus = previousStatus;
    }

    public StatusEnum getActualStatus() {
        return actualStatus;
    }

    public void setActualStatus(StatusEnum actualStatus) {
        this.actualStatus = actualStatus;
    }

    public LocalDateTime getMoveDate() {
        return moveDate;
    }

    public void setMoveDate(LocalDateTime moveDate) {
        this.moveDate = moveDate;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}
