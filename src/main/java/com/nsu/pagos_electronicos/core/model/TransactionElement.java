package com.nsu.pagos_electronicos.core.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
public class TransactionElement {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull @NotEmpty
    private String detalle;

    @NotNull
    private BigDecimal ammount;

    @NotNull
    private Integer quantity;


    @ManyToOne(fetch = FetchType.LAZY)
    private Transaction transaction;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public BigDecimal getAmmount() {
        return ammount;
    }

    public void setAmmount(BigDecimal price) {
        this.ammount = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}
