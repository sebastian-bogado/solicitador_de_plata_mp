package com.nsu.pagos_electronicos.core.model;

import lombok.Getter;

@Getter
public enum CurrencyEnum {

    ARS(""),
    USD("");

    private String messageCode;

    CurrencyEnum(String messageCode){
        this.messageCode = messageCode;
    }


}
