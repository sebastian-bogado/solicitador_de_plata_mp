package com.nsu.pagos_electronicos.core.model;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum StatusEnum {
    CREATED(""),
    PAUSED("");

    private String messageCode;

    StatusEnum(String code){
        this.messageCode=code;
    }


    public static StatusEnum getStatusEnumFromString(String name){
        for(StatusEnum statusEnum: Arrays.asList(StatusEnum.values())){
            if(statusEnum.name().equals(name)){
                return statusEnum;
            }
        }
        return null;
    }

}
