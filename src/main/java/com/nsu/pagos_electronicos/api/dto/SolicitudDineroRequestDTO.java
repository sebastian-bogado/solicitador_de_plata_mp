package com.nsu.pagos_electronicos.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nsu.pagos_electronicos.core.model.CurrencyEnum;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class SolicitudDineroRequestDTO {

    @JsonProperty("currency_id")
    private CurrencyEnum currency;

    @JsonProperty("payer_email")
    private String payerEmail;

    private BigDecimal amount;

    private String description;

    @JsonProperty("concept_type")
    private String concept;
}
