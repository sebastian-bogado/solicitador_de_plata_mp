package com.nsu.pagos_electronicos.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by sbogado on 7/10/17.
 */
@Data
public class SolicitudDineroResponseDTO {
    @JsonProperty("id")
    private String transactionId;
    @JsonProperty("status")
    private String transactionStatus;
    @JsonProperty("site_id")
    private String siteId;
    @JsonProperty("currency_id")
    private String currencyId;
    private BigDecimal amount;
    @JsonProperty("collector_id")
    private Long collectorId;
    @JsonProperty("collector_email")
    private String collectorEmail;
    @JsonProperty("payer_id")
    private Long payerId;
    @JsonProperty("payer_email")
    private String payerEmail;
    @JsonProperty("payer_phone")
    private String payerPhone;
    @JsonProperty("concept_type")
    private String concepType;
    @JsonProperty("init_point")
    private String initPoint;
    @JsonProperty("external_reference")
    private String externalReference;
    @JsonProperty("pref_id")
    private String prefId;
    @JsonProperty("date_created")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSXXX", timezone="America/Argentina/Buenos_Aires")
    private LocalDateTime dateCreated;

    @JsonProperty("last_updated")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSXXX", timezone="America/Argentina/Buenos_Aires")
    private LocalDateTime dateUpdated;
    private String source;

}
