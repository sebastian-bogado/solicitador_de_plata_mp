package com.nsu.pagos_electronicos.api.service;

import com.nsu.pagos_electronicos.api.dto.GenerateTokenResponseDTO;
import com.nsu.pagos_electronicos.api.dto.SolicitudDineroRequestDTO;
import com.nsu.pagos_electronicos.api.dto.SolicitudDineroResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class MercadoPagoServiceRest {


    @Autowired
    private RestTemplate restTemplate;


    private static final Logger LOG = LoggerFactory.getLogger(MercadoPagoServiceRest.class);

    @Value("${mercadoPago.url}")
    private String url;

    @Value("${mercadoPago.clientId}")
    private String clientId;

    @Value("${mercadoPago.clientSecret}")
    private String clientSecret;

    public SolicitudDineroResponseDTO createMoneyRequest(SolicitudDineroRequestDTO request) throws HttpClientErrorException {
        //TODO review this method
        final String finalUrl = url+"/money_requests?access_token="+this.generateToken().getAccessToken();
        LOG.info(finalUrl);
        LOG.info(request.toString());
        return restTemplate.postForObject(finalUrl, request, SolicitudDineroResponseDTO.class);
    }

    //TODO add cache
    public GenerateTokenResponseDTO generateToken(){
        LOG.info(url+"/oauth/token?grant_type=client_credentials&client_id="+this.clientId+"&client_secret="+this.clientSecret);
        return this.restTemplate.postForObject(url+"/oauth/token?grant_type=client_credentials&client_id="+this.clientId+"&client_secret="+this.clientSecret, null, GenerateTokenResponseDTO.class);
    }
}
